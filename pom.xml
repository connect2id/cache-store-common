<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <groupId>com.nimbusds</groupId>
    <artifactId>infinispan-cachestore-common</artifactId>
    <version>4.1-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>Infinispan Cache Store Common Classes</name>
    <description>Common classes and interfaces for Infinispan modules for persisting cache data</description>
    <url>https://bitbucket.org/connect2id/cache-store-common</url>

    <organization>
        <name>Connect2id Ltd.</name>
        <url>https://connect2id.com</url>
    </organization>

    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>https://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <scm>
        <connection>scm:git:https://bitbucket.org/connect2id/cache-store-common.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:connect2id/cache-store-common.git</developerConnection>
        <url>https://bitbucket.org/connect2id/cache-store-common</url>
        <tag>1.0.1</tag>
    </scm>

    <developers>
        <developer>
            <id>vdzhuvinov</id>
            <name>Vladimir Dzhuvinov</name>
            <email>vladimir@dzhuvinov.com</email>
        </developer>
    </developers>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.infinispan</groupId>
            <artifactId>infinispan-core</artifactId>
            <version>14.0.19.Final</version>
        </dependency>
        <dependency>
            <groupId>com.nimbusds</groupId>
            <artifactId>infinispan-cachestore-redis</artifactId>
            <version>11.0</version>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>com.github.stephenc.jcip</groupId>
            <artifactId>jcip-annotations</artifactId>
            <version>1.0-1</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.2</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.11.0</version>
                <configuration>
                    <source>17</source>
                    <target>17</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.3.0</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.5.0</version>
                <configuration>
                    <author>true</author>
                    <version>true</version>
                    <use>true</use>
                    <linksource>true</linksource>
                    <windowtitle>Infinispan cache store common classes v${project.version}</windowtitle>
                    <doctitle>Infinispan cache store common classes v${project.version}</doctitle>
                    <overview>${basedir}/src/main/javadoc/overview.html
                    </overview>
                </configuration>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.2.2</version>
                <configuration>
                    <systemProperties>
                        <property>
                            <name>log4j.configurationFile</name>
                            <value>log4j.xml</value>
                        </property>
                    </systemProperties>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>3.1.1</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>3.0.1</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
                <version>3.1.0</version>
                <executions>
                    <execution>
                        <id>sign-artifacts</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>sign</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>buildnumber-maven-plugin</artifactId>
                <version>3.1.0</version>
                <executions>
                    <execution>
                        <phase>validate</phase>
                        <goals>
                            <goal>create</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <getRevisionOnlyOnce>true</getRevisionOnlyOnce>
                    <doCheck>false</doCheck>
                    <doUpdate>false</doUpdate>
                    <timestampFormat>{0,date,yyyyMMdd.HHmmss.SSS}
                    </timestampFormat>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.sonatype.plugins</groupId>
                <artifactId>nexus-staging-maven-plugin</artifactId>
                <version>1.6.13</version>
                <extensions>true</extensions>
                <configuration>
                    <serverId>ossrh</serverId>
                    <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                    <autoReleaseAfterClose>true</autoReleaseAfterClose>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>
    
</project>
