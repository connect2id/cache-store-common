package com.nimbusds.infinispan.persistence.common.workarounds;


import net.jcip.annotations.ThreadSafe;
import org.infinispan.Cache;
import org.infinispan.persistence.redis.SimpleRedisStore;

import com.nimbusds.infinispan.persistence.common.InfinispanStore;


/**
 * Infinispan entry count resolver. Works around broken Infinispan wiring of
 * {@code Cache.size()} method when a cache store is configured.
 */
@ThreadSafe
public class EntryCountResolver {
	
	
	/**
	 * The count strategy.
	 */
	enum Strategy {
		DEFAULT,
		DIRECT,
		DIRECT_WITH_PASSIVATION,
		DIRECT_REDIS_STORE,
		DIRECT_REDIS_STORE_WITH_PASSIVATION
	}
	
	
	/**
	 * The chosen count strategy.
	 */
	private final Strategy strategy;
	
	
	/**
	 * The underlying Infinispan cache.
	 */
	private final Cache<?,?> cache;
	
	
	/**
	 * Infinispan store inheriting this library.
	 */
	private InfinispanStore<?,?> ispnStore;
	
	
	/**
	 * Infinispan Redis store.
	 */
	private SimpleRedisStore redisStore;
	
	
	/**
	 * Creates a new entry count resolver.
	 *
	 * @param cache The cache. Must not be {@code null}.
	 */
	public EntryCountResolver(final Cache<?,?> cache) {
		
		this.cache = cache;
		
		if (cache.getCacheConfiguration().persistence().usingStores()) {
			
			// Overflowing entries to store?
			final boolean passivationOn = cache.getCacheConfiguration().persistence().passivation();
			
			// Try with SQL, LDAP store and other stores that inherit from this lib
			ispnStore = InfinispanStore.getInstances().get(cache.getName());
			
			if (ispnStore != null) {
				strategy = passivationOn ? Strategy.DIRECT_WITH_PASSIVATION : Strategy.DIRECT;
				return;
			}
			
			// Try with modified Redis store (requires v8.1+)
			redisStore = SimpleRedisStore.getInstances().get(cache.getName());
			
			if (redisStore != null) {
				strategy = passivationOn ? Strategy.DIRECT_REDIS_STORE_WITH_PASSIVATION : Strategy.DIRECT_REDIS_STORE;
				return;
			}
		}
		
		strategy = Strategy.DEFAULT;
	}
	
	
	/**
	 * Returns the resolved strategy.
	 *
	 * @return The resolved strategy.
	 */
	public Strategy getStrategy() {
		
		return strategy;
	}
	
	
	/**
	 * Returns the cache size according to the resolved strategy.
	 *
	 * @return The cache size.
	 */
	public int getCount() {
		
		switch (strategy) {
			
			case DIRECT:
				return ispnStore.size();
			
			case DIRECT_WITH_PASSIVATION:
				// sum in-memory + passivated
				return cache.getAdvancedCache().getDataContainer().size() + ispnStore.size();
			
			case DIRECT_REDIS_STORE:
				return redisStore.size();
			
			case DIRECT_REDIS_STORE_WITH_PASSIVATION:
				// sum in-memory + passivated
				return cache.getAdvancedCache().getDataContainer().size() + redisStore.size();
			
			default:
				return cache.size();
		}
	}
}
