package com.nimbusds.infinispan.persistence.common.query;


/**
 * Unsupported query exception.
 */
public class UnsupportedQueryException extends RuntimeException {
	
	
	/**
	 * Creates a new unsupported query exception.
	 *
	 * @param query The unsupported query.
	 */
	public UnsupportedQueryException(final Query query) {
		super("Unsupported query: " + query);
	}
}
