package com.nimbusds.infinispan.persistence.common;


import java.time.Instant;

import net.jcip.annotations.Immutable;
import org.infinispan.metadata.InternalMetadata;


/**
 * Encapsulates an Infinispan entry consisting of a key / value pair and
 * associated optional metadata.
 */
@Immutable
public final class InfinispanEntry<K,V> {
	

	/**
	 * The entry key.
	 */
	private final K key;


	/**
	 * The entry value.
	 */
	private final V value;


	/**
	 * The optional entry metadata.
	 */
	private final InternalMetadata metadata;


	/**
	 * Creates a new Infinispan entry.
	 *
	 * @param key      The entry key. Must not be {@code null}.
	 * @param value    The entry value. {@code null} if none.
	 * @param metadata Optional entry metadata, {@code null} if not
	 *                 specified.
	 */
	public InfinispanEntry(final K key, final V value, final InternalMetadata metadata) {
		assert key != null;
		this.key = key;
		this.value = value;
		this.metadata = metadata;
	}


	/**
	 * Returns the entry key.
	 *
	 * @return The key.
	 */
	public K getKey() {
		return key;
	}


	/**
	 * Returns the entry value.
	 *
	 * @return The value, {@code null} if none.
	 */
	public V getValue() {
		return value;
	}


	/**
	 * Returns the optional entry metadata.
	 *
	 * @return The entry metadata, {@code null} if not specified.
	 */
	public InternalMetadata getMetadata() {
		return metadata;
	}
	
	
	/**
	 * Returns the entry creation timestamp.
	 *
	 * @return The entry creation Unix timestamp, in milliseconds, -1 if
	 *         not specified.
	 */
	public long created() {
		return metadata != null ? metadata.created() : -1L;
	}
	
	
	/**
	 * Returns the entry last use timestamp.
	 *
	 * @return The entry last use Unix timestamp, in milliseconds, -1 if not
	 *         specified.
	 */
	public long lastUsed() {
		return metadata != null ? metadata.lastUsed() : -1L;
	}
	
	
	/**
	 * Returns {@code true} if the entry is expired.
	 *
	 * @return {@code true} if the entry is expired.
	 */
	public boolean isExpired() {
		return isExpired(Instant.now());
	}
	
	
	/**
	 * Returns {@code true} if the entry is expired.
	 *
	 * @param now The current time instance.
	 *
	 * @return {@code true} if the entry is expired.
	 */
	public boolean isExpired(final Instant now) {
		return metadata != null && metadata.isExpired(now.toEpochMilli());
	}
}
