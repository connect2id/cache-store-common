package com.nimbusds.infinispan.persistence.common.query;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.jcip.annotations.Immutable;


/**
 * Simple match query.
 *
 * <pre>
 * key-A == value-A
 * </pre>
 */
@Immutable
public class SimpleMatchQuery<K,V> implements MatchQuery<K,V> {
	
	
	/**
	 * The key.
	 */
	private final K key;
	
	
	/**
	 * The value.
	 */
	private final V value;
	
	
	/**
	 * Creates a new simple match query with the specified key / value pair
	 * to match.
	 *
	 * @param key   The key. Must not be {@code null}.
	 * @param value The value. Must not be {@code null}.
	 */
	public SimpleMatchQuery(final K key, final V value) {
		if (key == null)
			throw new IllegalArgumentException("The key must not be null");
		this.key = key;
		if (value == null)
			throw new IllegalArgumentException("The value must not be null");
		this.value = value;
	}
	
	
	/**
	 * Returns the key to match.
	 *
	 * @return The key.
	 */
	public K getKey() {
		return key;
	}
	
	
	/**
	 * Returns the value to match.
	 *
	 * @return The value.
	 */
	public V getValue() {
		return value;
	}
	
	
	@Override
	public Map<K, V> getMatchMap() {
		Map<K,V> map = new HashMap<>();
		map.put(getKey(), getValue());
		return Collections.unmodifiableMap(map);
	}
	
	
	@Override
	public String toString() {
		return "[key=" + key + " value=" + value + "]";
	}
}
