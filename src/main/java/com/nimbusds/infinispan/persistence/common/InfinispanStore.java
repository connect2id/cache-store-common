package com.nimbusds.infinispan.persistence.common;


import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;

import com.nimbusds.infinispan.persistence.common.query.QueryExecutor;
import net.jcip.annotations.ThreadSafe;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.AdvancedLoadWriteStore;
import org.infinispan.persistence.spi.InitializationContext;


/**
 * Abstract persisting store for Infinispan 14.0+ caches and maps.
 */
@ThreadSafe
public abstract class InfinispanStore<K,V>
	implements AdvancedLoadWriteStore<K,V>, AdvancedCacheExpirationWriter<K,V> {
	
	
	/**
	 * The instances of this class, keyed by Infinispan cache name.
	 */
	private static Map<String,InfinispanStore> instances = new Hashtable<>();
	
	
	/**
	 * Returns the {@link #init initialised} instances of this class.
	 * Intended for testing and debugging purposes.
	 *
	 * @return The instances of this class, keyed by Infinispan cache name.
	 */
	public static Map<String,InfinispanStore> getInstances() {
		return Collections.unmodifiableMap(instances);
	}


	/**
	 * The initialisation context for the associated Infinispan cache /
	 * map.
	 */
	private InitializationContext ctx;
	
	
	/**
	 * The name of the associated Infinispan cache / map.
	 */
	private String cacheName;
	
	
	/**
	 * Returns the initialisation context for the associated Infinispan
	 * cache / map.
	 *
	 * @return The initialisation context for the associated Infinispan
	 *         cache / map.
	 */
	public InitializationContext getInitContext() {
		
		return ctx;
	}
	
	
	/**
	 * Returns the name of the associated Infinispan cache / map.
	 *
	 * @return The name of the associated Infinispan cache / map,
	 *         {@code null} if not available.
	 */
	public String getCacheName() {
		
		return cacheName;
	}
	

	@Override
	public void init(final InitializationContext ctx) {

		// This method will be invoked by the PersistenceManager during initialization. The InitializationContext
		// contains:
		// - this CacheLoader's configuration
		// - the cache to which this loader is applied. Your loader might want to use the cache's name to construct
		//   cache-specific identifiers
		// - the StreamingMarshaller that needs to be used to marshall/unmarshall the entries
		// - a TimeService which the loader can use to determine expired entries
		// - a ByteBufferFactory which needs to be used to construct ByteBuffers
		// - a MarshalledEntryFactory which needs to be used to construct entries from the data retrieved by the loader

		this.ctx = ctx;
		
		// Store cache name
		cacheName = ctx.getCache().getName();
		
		// Register store
		InfinispanStore.instances.put(cacheName, this);
	}
	
	
	/**
	 * Returns an query executor against the underlying Infinispan cache
	 * store.
	 *
	 * @return The query executor, {@code null} if not supported or not
	 *         enabled.
	 */
	public QueryExecutor<K,V> getQueryExecutor() {
		
		return null;
	}
	
	
	@Override
	public void stop() {
		
		// Unregister store
		if (cacheName != null) {
			InfinispanStore.instances.remove(cacheName);
		}
	}
}
