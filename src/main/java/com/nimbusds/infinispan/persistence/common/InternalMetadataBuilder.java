package com.nimbusds.infinispan.persistence.common;


import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.infinispan.container.versioning.EntryVersion;
import org.infinispan.metadata.InternalMetadata;
import org.infinispan.metadata.Metadata;


/**
 * Infinispan metadata builder. Can be used to reconstruct Infinispan metadata
 * for an entry.
 *
 * @deprecated Use {@link org.infinispan.metadata.EmbeddedMetadata}
 */
@Deprecated
public class InternalMetadataBuilder implements InternalMetadata.Builder {


	/**
	 * The entry creation time, as a Unix timestamp in milliseconds, -1 if
	 * not specified.
	 */
	private long created = -1L;


	/**
	 * The entry last use time, as a Unix timestamp in milliseconds, -1 if
	 * not specified.
	 */
	private long lastUsed = -1L;


	/**
	 * The entry lifespan, in milliseconds. -1 implies permanent.
	 */
	private long lifespan = -1L;


	/**
	 * The entry max idle time, in milliseconds. -1 implies none.
	 */
	private long maxIdle = -1L;


	/**
	 * The entry version, {@code null} if not specified.
	 */
	private EntryVersion entryVersion;


	/**
	 * Sets the entry creation timestamp.
	 *
	 * @param date The entry creation time, {@code null} if not specified.
	 *
	 * @return This builder.
	 */
	public InternalMetadataBuilder created(final Date date) {
		return created(date != null ? date.getTime() : -1L);
	}


	/**
	 * Sets the entry creation timestamp.
	 *
	 * @param l The entry creation Unix timestamp, in milliseconds, -1 if
	 *          not specified.
	 *
	 * @return This builder.
	 */
	public InternalMetadataBuilder created(final long l) {
		created = l;
		return this;
	}


	/**
	 * Sets the entry last use timestamp.
	 *
	 * @param date The entry last use time, {@code null} if not specified.
	 *
	 * @return This builder.
	 */
	public InternalMetadataBuilder lastUsed(final Date date) {
		return lastUsed(date != null ? date.getTime() : -1L);
	}


	/**
	 * Sets the entry last use timestamp.
	 *
	 * @param l The entry last use Unix timestamp, in milliseconds, -1 if
	 *          not specified.
	 *
	 * @return This builder.
	 */
	public InternalMetadataBuilder lastUsed(final long l) {
		lastUsed = l;
		return this;
	}


	@Override
	public InternalMetadataBuilder lifespan(final long l, TimeUnit timeUnit) {
		if (l < 0) {
			lifespan = -1L;
		} else {
			lifespan = timeUnit.toMillis(l);
		}

		return this;
	}


	@Override
	public InternalMetadataBuilder lifespan(final long l) {
		lifespan = l;
		return this;
	}


	@Override
	public InternalMetadataBuilder maxIdle(final long l, TimeUnit timeUnit) {
		if (l < 0) {
			maxIdle = -1L;
		} else {
			maxIdle = timeUnit.toMillis(l);
		}

		return this;
	}


	@Override
	public InternalMetadataBuilder maxIdle(final long l) {
		maxIdle = l;
		return this;
	}


	@Override
	public InternalMetadataBuilder version(final EntryVersion entryVersion) {
		this.entryVersion = entryVersion;
		return this;
	}


	@Override
	public InternalMetadata build() {

		final InternalMetadataBuilder builder = this;

		return new InternalMetadata() {
			@Override
			public long created() {
				return created;
			}


			@Override
			public long lastUsed() {
				return lastUsed;
			}


			@Override
			public boolean isExpired(long now) {
				// Copied from private class org.infinispan.metadata.impl.InternalMetadataImpl
				long expiry = expiryTime();
				return expiry > 0 && expiry <= now;
			}


			@Override
			public long expiryTime() {
				// Copied from private class org.infinispan.metadata.impl.InternalMetadataImpl
				long lset = lifespan > -1 ? created + lifespan : -1;
				long muet = maxIdle > -1 ? lastUsed + maxIdle : -1;
				if (lset == -1) return muet;
				if (muet == -1) return lset;
				return Math.min(lset, muet);
			}


			@Override
			public long lifespan() {
				return lifespan;
			}


			@Override
			public long maxIdle() {
				return maxIdle;
			}


			@Override
			public EntryVersion version() {
				return entryVersion;
			}


			@Override
			public Builder builder() {
				return builder;
			}
		};
	}


	@Override
	public Metadata.Builder merge(final Metadata metadata) {

		// Copied from org.infinispan.metadata.EmbeddedMetadata

		if (lifespan == -1L) { // if lifespan not set, apply default
			lifespan = metadata.lifespan();
		}

		if (maxIdle == -1L) { // if maxIdle not set, apply default
			maxIdle = metadata.maxIdle();
		}

		if (entryVersion == null)
			entryVersion = metadata.version();

		return this;
	}
}
