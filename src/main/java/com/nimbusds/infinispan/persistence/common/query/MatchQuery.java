package com.nimbusds.infinispan.persistence.common.query;


import java.util.Map;


/**
 * Match query.
 */
public interface MatchQuery<K,V> extends Query {
	
	
	/**
	 * Returns the key / value pairs to match.
	 *
	 * @return The key / value match map.
	 */
	Map<K,V> getMatchMap();
}
