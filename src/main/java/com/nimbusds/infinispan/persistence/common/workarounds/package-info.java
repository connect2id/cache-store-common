/**
 * Infinispan workarounds.
 */
package com.nimbusds.infinispan.persistence.common.workarounds;