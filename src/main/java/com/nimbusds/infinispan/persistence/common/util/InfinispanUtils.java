package com.nimbusds.infinispan.persistence.common.util;


import org.infinispan.commons.io.ByteBuffer;
import org.infinispan.metadata.InternalMetadata;
import org.infinispan.metadata.Metadata;
import org.infinispan.metadata.impl.PrivateMetadata;
import org.infinispan.persistence.spi.MarshallableEntry;
import org.infinispan.persistence.spi.MarshalledValue;


/**
 * Infinispan utilities.
 */
public class InfinispanUtils {
	
	
	/**
	 * Returns a new marshallable entry for the specified key and value.
	 *
	 * @param key   The key. Must not be {@code null}.
	 * @param value The value, {@code null} if not specified.
	 *
	 * @return The marshallable entry.
	 */
	public static <K,V> MarshallableEntry<K, V> toMarshallableEntry(final K key, final V value) {
		
		return toMarshallableEntry(key, value, null);
	}
	
	
	/**
	 * Returns a new marshallable entry for the specified key, value and
	 * internal metadata.
	 *
	 * @param key              The key. Must not be {@code null}.
	 * @param value            The value, {@code null} if not specified.
	 * @param internalMetadata The internal metadata, {@code null} if not
	 *                         specified.
	 *
	 * @return The marshallable entry.
	 */
	public static <K,V> MarshallableEntry<K, V> toMarshallableEntry(final K key,
									final V value,
									final InternalMetadata internalMetadata) {
	
		return new MarshallableEntry<>() {
			@Override
			public ByteBuffer getKeyBytes() {
				throw new UnsupportedOperationException();
			}
			
			
			@Override
			public ByteBuffer getValueBytes() {
				throw new UnsupportedOperationException();
			}
			
			
			@Override
			public ByteBuffer getMetadataBytes() {
				throw new UnsupportedOperationException();
			}
			
			
			@Override
			public ByteBuffer getInternalMetadataBytes() {
				throw new UnsupportedOperationException();
			}
			
			
			@Override
			public K getKey() {
				return key;
			}
			
			
			@Override
			public V getValue() {
				return value;
			}
			
			
			@Override
			public Metadata getMetadata() {
				return internalMetadata;
			}
			
			
			@Override
			public PrivateMetadata getInternalMetadata() {
				return null;
			}
			
			
			@Override
			public long created() {
				return internalMetadata != null ? internalMetadata.created() : -1L;
			}
			
			
			@Override
			public long lastUsed() {
				return internalMetadata != null ? internalMetadata.lastUsed() : -1L;
			}
			
			
			@Override
			public boolean isExpired(long now) {
				return internalMetadata != null && internalMetadata.isExpired(now);
			}
			
			
			@Override
			public long expiryTime() {
				return internalMetadata != null ? internalMetadata.expiryTime() : -1;
			}
			
			
			@Override
			public MarshalledValue getMarshalledValue() {
				throw new UnsupportedOperationException();
			}
		};
	}
	
	
	private InfinispanUtils() {}
}
