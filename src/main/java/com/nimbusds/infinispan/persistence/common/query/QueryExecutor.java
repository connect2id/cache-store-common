package com.nimbusds.infinispan.persistence.common.query;


import java.util.function.Consumer;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;


/**
 * Interface for executing queries directly against a particular Infinispan
 * cache store (bypassing the Infinispan
 * {@link org.infinispan.persistence.spi.AdvancedCacheLoader} APIs).
 */
public interface QueryExecutor<K,V> {
	
	
	/**
	 * Executes the specified query.
	 *
	 * @param query    The query. Must not be {@code null}.
	 * @param consumer The result consumer. Must not be {@code null}.
	 */
	void executeQuery(final Query query, final Consumer<InfinispanEntry<K,V>> consumer);
}
