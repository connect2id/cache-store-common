package com.nimbusds.infinispan.persistence.common.query;


import java.util.Map;

import net.jcip.annotations.Immutable;


/**
 * Multi-match query.
 *
 * <pre>
 * key-A == value-A &amp;&amp; key-B == value-B &amp;&amp; ...
 * </pre>
 */
@Immutable
public class MultiMatchQuery<K,V> implements MatchQuery<K,V> {
	
	
	/**
	 * The key / value map.
	 */
	private final Map<K,V> matchMap;
	
	
	/**
	 * Creates a new multi-match query with the specified key / value pairs
	 * to match.
	 *
	 * @param matchMap The key / value match map. Must not be {@code null}.
	 */
	public MultiMatchQuery(final Map<K,V> matchMap) {
		
		if (matchMap == null)
			throw new IllegalArgumentException("The key / value map must not be null");
		
		this.matchMap = matchMap;
	}
	
	
	@Override
	public Map<K,V> getMatchMap() {
		return matchMap;
	}
	
	
	@Override
	public String toString() {
		return matchMap.toString();
	}
}
