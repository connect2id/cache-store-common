package com.nimbusds.infinispan.persistence.common.query;


import java.util.Collections;
import java.util.HashMap;

import junit.framework.TestCase;


public class MultiMatchQueryTest extends TestCase {
	
	
	public void testEmpty() {
		
		var query = new MultiMatchQuery<>(Collections.emptyMap());
		assertTrue(query.getMatchMap().isEmpty());
	}
	
	
	public void testOnePair() {
		
		var matches = new HashMap<>();
		matches.put("k", "v");
		var query = new MultiMatchQuery<>(matches);
		assertEquals(matches, query.getMatchMap());
		assertTrue(query instanceof MatchQuery);
		assertTrue(query instanceof Query);
		assertEquals("{k=v}", query.toString());
	}
	
	
	public void testTwoPairs() {
		
		var matches = new HashMap<>();
		matches.put("k-1", "v-1");
		matches.put("k-2", "v-2");
		var query = new MultiMatchQuery<>(matches);
		assertEquals(matches, query.getMatchMap());
		assertTrue(query instanceof MatchQuery);
		assertTrue(query instanceof Query);
	}
	
	
	public void testRejectNullMap() {
		
		try {
			new MultiMatchQuery<String,String>(null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The key / value map must not be null", e.getMessage());
		}
	}
}
