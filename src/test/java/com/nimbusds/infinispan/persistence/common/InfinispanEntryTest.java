package com.nimbusds.infinispan.persistence.common;


import java.time.Instant;

import junit.framework.TestCase;
import org.infinispan.metadata.InternalMetadata;


public class InfinispanEntryTest extends TestCase {
	

	public void testWithoutMetadata() {
		
		var entry = new InfinispanEntry<>("key", "value", null);
		assertEquals("key", entry.getKey());
		assertEquals("value", entry.getValue());
		assertNull(entry.getMetadata());
		assertEquals(-1L, entry.created());
		assertEquals(-1L, entry.lastUsed());
		assertFalse(entry.isExpired());
	}
	
	
	public void testWithDefaultMetadata() {
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.build();
		
		var entry = new InfinispanEntry<>("key", "value", metadata);
		assertEquals("key", entry.getKey());
		assertEquals("value", entry.getValue());
		assertEquals(metadata, entry.getMetadata());
		assertEquals(-1L, entry.created());
		assertEquals(-1L, entry.lastUsed());
		assertFalse(entry.isExpired());
	}
	
	
	public void testWithFullySetMetadata() {
		
		long created = Instant.now().toEpochMilli();
		long lastUsed = created + 1000L;
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(created)
			.lifespan(20_000L)
			.maxIdle(5_000L)
			.lastUsed(lastUsed)
			.build();
		
		var entry = new InfinispanEntry<>("key", "value", metadata);
		assertEquals("key", entry.getKey());
		assertEquals("value", entry.getValue());
		assertEquals(metadata, entry.getMetadata());
		assertEquals(created, entry.created());
		assertEquals(lastUsed, entry.lastUsed());
		assertFalse(entry.isExpired());
	}
	
	
	public void testIsExpired_neverIfNoMetadata() {
		
		var entry = new InfinispanEntry<>("key", "value", null);
		
		assertFalse(entry.isExpired());
		assertFalse(entry.isExpired(Instant.now()));
	}
	
	
	public void testIsExpired_neverIfDefaultMetadata() {
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.build();
		
		var entry = new InfinispanEntry<>("key", "value", metadata);
		
		assertFalse(entry.isExpired());
		assertFalse(entry.isExpired(Instant.now()));
	}
	
	
	public void testIsExpired_negative() {
		
		long created = Instant.now().toEpochMilli();
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(created)
			.lifespan(1000L)
			.build();
		
		var entry = new InfinispanEntry<>("key", "value", metadata);
		
		assertEquals(created, entry.created());
		assertEquals(-1L, entry.lastUsed());
		assertFalse(entry.isExpired());
		assertFalse(entry.isExpired(Instant.now()));
	}
	
	
	public void testIsExpired_positive() {
		
		long created = Instant.now().toEpochMilli();
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(created)
			.lifespan(1000L)
			.build();
		
		var entry = new InfinispanEntry<>("key", "value", metadata);
		
		assertEquals(created, entry.created());
		assertEquals(-1L, entry.lastUsed());
		assertFalse(entry.isExpired());
		assertFalse(entry.isExpired(Instant.now()));
	}
}
