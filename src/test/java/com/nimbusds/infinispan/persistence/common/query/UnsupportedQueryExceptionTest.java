package com.nimbusds.infinispan.persistence.common.query;


import junit.framework.TestCase;


public class UnsupportedQueryExceptionTest extends TestCase {
	

	public void testMessage() {
		
		UnsupportedQueryException e = new UnsupportedQueryException(new SimpleMatchQuery<>(100, "hundred"));
		
		assertEquals("Unsupported query: [key=100 value=hundred]", e.getMessage());
	}
}
