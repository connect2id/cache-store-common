package com.nimbusds.infinispan.persistence.common.query;


import junit.framework.TestCase;


public class SimpleMatchQueryTest extends TestCase {
	

	public void testConstructor() {
		
		SimpleMatchQuery<Integer,String> query = new SimpleMatchQuery<>(100, "hundred");
		assertEquals(Integer.valueOf(100), query.getKey());
		assertEquals("hundred", query.getValue());
		assertEquals("[key=100 value=hundred]", query.toString());
		assertEquals("hundred", query.getMatchMap().get(100));
		assertEquals(1, query.getMatchMap().size());
		
		assertTrue(query instanceof MatchQuery);
		assertTrue(query instanceof Query);
	}
	
	public void testRejectNullKey() {
		
		try {
			new SimpleMatchQuery<Integer,String>(null, "hundred");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The key must not be null", e.getMessage());
		}
	}
	
	public void testRejectNullValue() {
		
		try {
			new SimpleMatchQuery<Integer,String>(100, null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The value must not be null", e.getMessage());
		}
	}
}
