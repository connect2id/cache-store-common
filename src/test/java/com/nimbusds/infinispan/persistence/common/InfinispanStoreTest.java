package com.nimbusds.infinispan.persistence.common;


import junit.framework.TestCase;


public class InfinispanStoreTest extends TestCase {
	

	public void testInstances() {
		
		assertTrue(InfinispanStore.getInstances().isEmpty());
	}
	

	public void testInstancesImmutable() {
		
		try {
			InfinispanStore.getInstances().put("cache-name", (InfinispanStore)null);
			fail();
		} catch (UnsupportedOperationException e) {
			assertNull(e.getMessage());
		}
	}
}
