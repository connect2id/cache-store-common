package com.nimbusds.infinispan.persistence.common;


import java.time.Instant;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;
import org.infinispan.container.versioning.EntryVersion;
import org.infinispan.container.versioning.InequalVersionComparisonResult;
import org.infinispan.metadata.InternalMetadata;


public class InternalMetadataBuilderTest extends TestCase {


	private static class Version implements EntryVersion {

		@Override
		public InequalVersionComparisonResult compareTo(EntryVersion entryVersion) {
			return null;
		}
	}
	

	public void testDefault() {

		var builder = new InternalMetadataBuilder();

		InternalMetadata metadata = builder.build();

		assertEquals(-1L, metadata.created());
		assertEquals(-1L, metadata.lastUsed());
		assertEquals(-1L, metadata.lifespan());
		assertEquals(-1L, metadata.maxIdle());
		assertNull(metadata.version());
		assertEquals(-1L, metadata.expiryTime());
		assertFalse(metadata.isExpired(new Date().getTime()));
		assertEquals(builder, metadata.builder());
	}


	public void testAllSet_dateTimestamps() {

		final Date now = new Date();
		final Date yesterday = new Date(now.getTime() - 24*60*60*1000);
		final Date oneHourAgo = new Date(now.getTime() - 60*60*1000);
		final long oneWeek = 7*24*60*60*1000;
		final long halfHour = 30*60*1000;
		final EntryVersion version = new Version();

		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(yesterday)
			.lastUsed(oneHourAgo)
			.lifespan(oneWeek)
			.maxIdle(halfHour)
			.version(version)
			.build();

		assertEquals(yesterday.getTime(), metadata.created());
		assertEquals(oneHourAgo.getTime(), metadata.lastUsed());
		assertEquals(oneWeek, metadata.lifespan());
		assertEquals(halfHour, metadata.maxIdle());
		assertEquals(version, metadata.version());
		assertTrue(metadata.expiryTime() < now.getTime());
		assertTrue(metadata.isExpired(now.getTime()));
	}


	public void testAllSet_longTimestamps() {

		final Date now = new Date();
		final Date yesterday = new Date(now.getTime() - 24*60*60*1000);
		final Date oneHourAgo = new Date(now.getTime() - 60*60*1000);
		final long oneWeek = 7*24*60*60*1000;
		final long halfHour = 30*60*1000;
		final EntryVersion version = new Version();

		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(yesterday.getTime())
			.lastUsed(oneHourAgo.getTime())
			.lifespan(oneWeek)
			.maxIdle(halfHour)
			.version(version)
			.build();

		assertEquals(yesterday.getTime(), metadata.created());
		assertEquals(oneHourAgo.getTime(), metadata.lastUsed());
		assertEquals(oneWeek, metadata.lifespan());
		assertEquals(halfHour, metadata.maxIdle());
		assertEquals(version, metadata.version());
		assertTrue(metadata.expiryTime() < now.getTime());
		assertTrue(metadata.isExpired(now.getTime()));
	}


	public void testSettersWithTimeUnitConversion() {

		final Date now = new Date();
		final Date yesterday = new Date(now.getTime() - 24*60*60*1000);
		final Date oneHourAgo = new Date(now.getTime() - 60*60*1000);
		final long oneWeek = 7*24*60*60*1000;
		final long halfHour = 30*60*1000;
		final EntryVersion version = new Version();

		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(yesterday.getTime())
			.lastUsed(oneHourAgo.getTime())
			.lifespan(7L, TimeUnit.DAYS)
			.maxIdle(30L, TimeUnit.MINUTES)
			.version(version)
			.build();

		assertEquals(yesterday.getTime(), metadata.created());
		assertEquals(oneHourAgo.getTime(), metadata.lastUsed());
		assertEquals(oneWeek, metadata.lifespan());
		assertEquals(halfHour, metadata.maxIdle());
		assertEquals(version, metadata.version());
		assertTrue(metadata.expiryTime() < now.getTime());
		assertTrue(metadata.isExpired(now.getTime()));
	}


	public void testDateTimestamps_nullSet() {

		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(null)
			.lastUsed(null)
			.build();

		assertEquals(-1L, metadata.created());
		assertEquals(-1L, metadata.lastUsed());
	}
	
	
	public void testCalculateExpTime() {
		
		Instant now = Instant.now();
		
		System.out.println("Now time: " + now);
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(now.toEpochMilli())
			.lifespan(1L, TimeUnit.SECONDS)
			.build();
		
		System.out.println("Exp time: " + Instant.ofEpochMilli(metadata.expiryTime()));
	}
}
