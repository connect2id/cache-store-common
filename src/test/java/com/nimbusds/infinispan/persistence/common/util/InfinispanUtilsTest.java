package com.nimbusds.infinispan.persistence.common.util;


import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;
import org.infinispan.metadata.InternalMetadata;
import org.infinispan.persistence.spi.MarshallableEntry;

import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;


public class InfinispanUtilsTest extends TestCase {

	
	public void testToMarshallableEntry_withKeyOnly() {
		
		String key = "1";
		
		MarshallableEntry<String, Object> entry = InfinispanUtils.toMarshallableEntry(key, null);
		
		assertEquals(key, entry.getKey());
		assertNull(entry.getValue());
		assertNull(entry.getMetadata());
		assertNull(entry.getInternalMetadata());
		assertEquals(-1L, entry.created());
		assertEquals(-1L, entry.lastUsed());
		assertFalse(entry.isExpired(Instant.now().toEpochMilli()));
		assertEquals(-1L, entry.expiryTime());
		
		try {
			entry.getKeyBytes();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
		
		try {
			entry.getValueBytes();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
		
		try {
			entry.getMetadataBytes();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
		
		try {
			entry.getInternalMetadataBytes();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
		
		try {
			entry.getMarshalledValue();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
	}

	
	public void testToMarshallableEntry_withKeyAndValue() {
		
		String key = "1";
		String value = "a";
		
		MarshallableEntry<String, Object> entry = InfinispanUtils.toMarshallableEntry(key, value);
		
		assertEquals(key, entry.getKey());
		assertEquals(value, entry.getValue());
		assertNull(entry.getMetadata());
		assertNull(entry.getInternalMetadata());
		assertEquals(-1L, entry.created());
		assertEquals(-1L, entry.lastUsed());
		assertFalse(entry.isExpired(Instant.now().toEpochMilli()));
		assertEquals(-1L, entry.expiryTime());
		
		try {
			entry.getKeyBytes();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
		
		try {
			entry.getValueBytes();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
		
		try {
			entry.getMetadataBytes();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
		
		try {
			entry.getInternalMetadataBytes();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
		
		try {
			entry.getMarshalledValue();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
	}


	public void testToMarshallableEntry_withKeyAndValueAndMetadata() {
		
		String key = "1";
		String value = "a";
		long created = Instant.now().minus(1, ChronoUnit.DAYS).toEpochMilli();
		long lastUsed = Instant.now().minus(1, ChronoUnit.MINUTES).toEpochMilli();
		long lifeSpan = TimeUnit.DAYS.toMillis(30);
		long maxIdle = TimeUnit.DAYS.toMillis(3);
		
		InternalMetadata internalMetadata = new InternalMetadataBuilder()
			.created(created)
			.lastUsed(lastUsed)
			.lifespan(lifeSpan)
			.maxIdle(maxIdle)
			.build();
		
		MarshallableEntry<String, Object> entry = InfinispanUtils.toMarshallableEntry(key, value, internalMetadata);
		
		assertEquals(key, entry.getKey());
		assertEquals(value, entry.getValue());
		assertEquals(internalMetadata, entry.getMetadata());
		assertNull(entry.getInternalMetadata());
		assertEquals(created, entry.created());
		assertEquals(lastUsed, entry.lastUsed());
		assertFalse(entry.isExpired(Instant.now().toEpochMilli()));
		assertEquals(lastUsed + maxIdle, entry.expiryTime());
		
		try {
			entry.getKeyBytes();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
		
		try {
			entry.getValueBytes();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
		
		try {
			entry.getMetadataBytes();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
		
		try {
			entry.getInternalMetadataBytes();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
		
		try {
			entry.getMarshalledValue();
			fail();
		} catch (UnsupportedOperationException e) {
			// ok
		}
	}
}
